const express = require('express');
const app = express();

const bodyParser = require('body-parser');
const routes = require('./routes/index');
const getTasks = require('./routes/getTasks');
const createTask = require('./routes/createTask');
const updateTask = require('./routes/updateTask');
const deleteTask = require('./routes/deleteTask');
const users = require('./routes/users');
const cors = require('cors');

app.use(cors());
app.use(bodyParser.urlencoded({
	extended: true
}));
app.use(bodyParser.json());
app.use(routes);
app.use(getTasks);
app.use(createTask);
app.use(updateTask);
app.use(deleteTask);
app.use(users);

app.listen(3000, () => console.log('App listening on port 3000!'));