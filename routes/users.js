const uniqid = require('uniqid');
const express = require('express');
const router = express.Router();
const data = require('../database');

router.post('/users', function(req, res) {
	const id = uniqid();
	data.usersId.push(id);
	data.tasks.push({
		userId: id,
		tasks: []
	});
	res.setHeader('Access-Control-Allow-Origin', '*');
	res.setHeader('Content-Type', 'application/json');
	res.send(JSON.stringify({
		id : id
	}));
});

module.exports = router;