const express = require('express');
const router = express.Router();
const data = require('../database');

router.delete('/:userId/tasks/:taskId', function(req, res) {
    let findUser = false;
    let findtask = false;

    for (let task of data.tasks) {
        if (task.userId === req.params.userId) {
            findUser = true;
            for (let [index, t] of task.tasks.entries()){
                if (t.id === req.params.taskId) {
                    findtask = true;
                    task.tasks.splice(index, 1);
                    res.setHeader('Content-Type', 'text/plain');
                    res.status(204).send();
                    break;
                }
            }
            if (!findtask){
                res.setHeader('Content-Type', 'text/html');
                res.status(400).send(`Task with id \'${req.params.taskId}\' doesn\'t exist.`);
            }
            break;
        }
    }
    if (!findUser) {
        res.setHeader('Content-Type', 'text/html');
        res.status(400).send(`User with id \'${req.params.userId}\' doesn\'t exist.`);
    }
});

module.exports = router;