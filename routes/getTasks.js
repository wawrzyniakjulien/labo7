
const express = require('express');
const router = express.Router();
const data = require('../database');

router.get('/:userId/tasks', function(req, res) {
	let find = false;
	data.tasks.forEach((task) => {
		if (task.userId === req.params.userId) {
			res.setHeader('Content-Type', 'application/json');
			const tasks = {
				tasks: task.tasks
			};
			find = true;
			res.send(JSON.stringify(tasks));
		}
	});
	if (!find)
		res.status(404).send('User not found');
});

module.exports = router;