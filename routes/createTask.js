const uniqid = require('uniqid');
const express = require('express');
const router = express.Router();
const data = require('../database');

router.post('/:userId/tasks', function(req, res) {
	let find = false;

	const idTask = uniqid();
	const taskObj = {
		id: idTask,
		name: req.body.name
	};
	data.tasks.forEach((task) => {
		if (task.userId === req.params.userId) {
			find = true;
			task.tasks.push(taskObj);
			res.setHeader('Content-Type', 'application/json');
			res.send(JSON.stringify(taskObj));
		}
	});
	if (!find)
		res.status(404).send('User not found');
});

module.exports = router;