const express = require('express');
const router = express.Router();
const data = require('../database');

router.put('/:userId/tasks/:taskId', function(req, res) {
    let findUser = false;
    let findtask = false;

    for (let task of data.tasks) {
        if (task.userId === req.params.userId) {
            findUser = true;
            for (let t of task.tasks){
                if (t.id === req.params.taskId) {
                    findtask = true;
                    t.name = req.body.name;
                    res.setHeader('Content-Type', 'text/html');
                    res.status(200).send(JSON.stringify(t));
                    break;
                }
            }
            if (!findtask){
                res.setHeader('Content-Type', 'text/html');
                res.status(400).send(`Task with id \'${req.params.taskId}\' doesn\'t exist.`);
            }
            break;
        }
    }
    if (!findUser) {
        res.setHeader('Content-Type', 'text/html');
        res.status(400).send(`User with id \'${req.params.userId}\' doesn\'t exist.`);
    }
});

module.exports = router;
